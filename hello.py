from flask import Flask, request  # pip install flask
import time
import redis

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

@app.route("/", methods=['POST'])
def hello2():
    request.get_data()
    data = request.data
    key = time.time()
    cache.set(key,data)
    #file = open("data.txt","a")
    #file.write(data.decode("utf-8") + "\n")
    #file.close()
    return data
"""
def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)
"""
@app.route('/')
def hello():
   # count = get_hit_count()
   # return 'Hello from Docker! I have been seen {} times.\n'.format(count)
    result=""
    for key in cache.scan_iter():        
        result+=key.decode('utf-8')+": "+(cache.get(key)).decode('utf-8')+"\n"
    return result
    #no comments

    
if __name__ == "__main__":
    app.run(host='0.0.0.0')