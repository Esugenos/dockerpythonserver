FROM python
COPY hello.py /hello.py
RUN pip3 install Flask
RUN pip install redis
CMD python3 /hello.py